#ifndef IMAGEM_PGM_HPP
#define IMAGEM_PGM_HPP
#include<iostream>
#include "imagem.hpp"
using namespace std;
class ImagemPgm : public Imagem {

	private:
	char byteFoto;
	char byteExtraido;
	char caracterExtraido;

	public:
	ImagemPgm();
	void setByteFoto(char byteFoto);
	char getByteFoto();
    void setByteExtraido(char byteExtraido);
    char getByteExtraido();
    void setCaracterExtraido(char caracterExtraido);
    char getCaracterExtraido();
    void setResolveImagem();


};
#endif



