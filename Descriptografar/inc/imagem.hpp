#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include<iostream>
#include<fstream>

using namespace std;

class Imagem{

	protected:
	string numeroMagico;
	string mensagemNaSegundaLinha;
	int colunas;
	int linhas;
	int pixel;
	//ifstream imagem;
    string arquivo;
	string parteDoArquivo;

	public:
	Imagem();
	void setNumeroMagico(string numeroMagico);
	string getNumeroMagico();
    void setMensagemNaSegundaLinha(string mensagemNaSegundaLinha);
    string getMensagemNaSegundaLinha();
	void setColunas(int colunas);
	int getColunas();
    void setLinhas(int linhas);
    int getLinhas();
    void setPixel(int pixel);
    int getPixel();
    protected:
    void setResolveImagem();
    //void setImagem(ifstream imagem);
   // ifstream getImagem();
    //void setArquivo(string arquivo);
   // string getArquivo();
   // void setParteDoArquivo(string parteDoArquivo);
   // string getParteDoArquivo();
};
#endif


